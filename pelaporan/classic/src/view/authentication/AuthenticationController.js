Ext.define('Admin.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',

    //TODO: implement central Facebook OATH handling here

    onEmailLogin : function() { // langsung menuju ke menu email
        this.redirectTo('email', true);
    },

    onLoginButton: function() { // langsung menuju ke menu dasboard
        this.redirectTo('dashboard', true);
    },

    onLoginAsButton: function() {
        this.redirectTo('login', true);
    },

    onNewAccount:  function() { // langsung menuju ke menu register
        this.redirectTo('register', true);
    },

    onSignupClick:  function() {
        this.redirectTo('login', true);
        
    },
    onFaceBookLogin:  function() {
        this.redirectTo('email', true);
        
    },

    onResetClick:  function() {
        this.redirectTo('lockscreen', true);
    },
    onLockScreenClick:  function() {
        this.redirectTo('login', true);
    }
});
Ext.define('Admin.view.store.Anggota', {
    extend: 'Ext.data.Store',

    alias: 'store.anggota',

    fields: [
        'tugas', 'tanggalupload','nilai'
    ],

    data: { items: [
        { tugas: 'Administrasi', tanggalupload: "01-02-2017", nilai:'80'},
        { tugas: 'Pembuatan Aplikasi', tanggalupload: "08-03-2017", nilai:'75'},
        { tugas: 'Pembuatan Absen', tanggalupload: "15-04-2017", nilai:'85'},
        { tugas: 'Web Aplikasi Online', tanggalupload: "10-05-2017", nilai:'95'}
        
    ]},

    //sebagai penghubung dari anggota ke user profile
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});

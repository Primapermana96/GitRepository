// untuk memanggil kerangkanya saja
Ext.define('Admin.view.profile.UserProfile', {
    extend: 'Admin.view.profile.UserProfileBase',
    xtype: 'profile', // sebagai pin untuk di panggil di navigation tree
    cls: 'userProfile-container',

    // sebagai ekstensi tools untuk fungsi-fungsi di dalam ini
    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Admin.view.store.Anggota'
    ],

    //untuk mengatur posisi dan tipe dalam tampilan templates
    layout: 'responsivecolumn',

    items: [
        {
            xtype: 'profilesocial',
            
            // Use 50% of container when viewport is big enough, 100% otherwise
            userCls: 'big-50 small-100 shadow'
        },
        {
            xtype: 'profiledescription',

            userCls: 'big-50 small-100 shadow'
        },
        {
        title: 'Data Lampiran', // judul
        width: 520,
        height:300,
        // untuk menentukkan tema yang di sisipkan 
        xtype: 'grid',
        // panggil data dari store anggota   
        store: {
            type: 'anggota'
        },
        tbar: [
        {   //text box
            xtype: 'filefield',
            hideLabel: true,
            emptyText:'Upload File PDF',
            width:200,
            listeners: {
                change: 'buttonOnlyChange' //langsung membuka folder yang ingin di upload
            }
        },
        // textbox searching browser
        {
            xtype: 'combo', //kotak-kotak
            store: {
                type: 'anggota',
                pageSize: 10
            },
            style:'margin-Justify:0.5%',
            displayField: 'title',
            typeAhead: true,
            emptyText:'Searching',
            hideLabel: true,
            hideTrigger:true,
            anchor: '100%',
        }
        
        ],
        // struktur pada konten yang akan di tampilkan
        columns: [
            {xtype: 'rownumberer'}, //untuk memberikan nomor urut secara otomatis
            {
                text: 'NamaTugas', // struktur tabel
                dataIndex: 'tugas' // manggil data
            }, {
                text: 'TanggalUpload', // struktur tabel
                dataIndex: 'tanggalupload' // manggil data
                
            },
            {
                text: 'Nilai', // struktur tabel
                dataIndex: 'nilai' // manggil data
                
            }
        ]
        }
    ]
});

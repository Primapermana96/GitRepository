//  untuk mengatur jumlah pada menu

Ext.define('Admin.store.NavigationTree', {
    extend: 'Ext.data.TreeStore', // untuk memasukkan fungsi treestore

    storeId: 'NavigationTree', // sebagai pin store yang di panggil untuk di tampilkan


    root: {
        expanded: true,
        children: [

            {
                viewType: 'login', // untuk memanggil form dari xtype 
                routeId: 'login', // untuk meroot tampilan awal
                // routeId defaults to viewType
            },
             {
                viewType: 'passwordreset', // untuk memanggil form dari xtype 
                routeId: 'passwordreset', // untuk meroot tampilan awal
                // routeId defaults to viewType
            },
             {
                text: 'Dashboard', // untuk teks
                iconCls: 'x-fa fa-desktop',// sebagai iconnya 
                rowCls: 'nav-tree-badge nav-tree-badge-new', // sebagai icon
                viewType: 'profile', // untuk memanggil form dari xtype 
                routeId: 'dashboard', // untuk meroot tampilan awal
                // routeId defaults to viewType
            },
             {
                text: 'Email', // untuk teks
                iconCls: 'x-fa fa-inbox',// sebagai iconnya 
                rowCls: 'nav-tree-badge nav-tree-badge-new', // sebagai icon
                viewType: 'email', // untuk memanggil form dari xtype
                routeId: 'email', // untuk meroot tampilan awal
                // routeId defaults to viewType
            },
            
            {
                viewType: 'register', // untuk memanggil form dari xtype 
                routeId: 'register', // untuk meroot tampilan awal
                // routeId defaults to viewType
            },
            {
                viewType: 'lockscreen', // untuk memanggil form dari xtype 
                routeId: 'lockscreen', // untuk meroot tampilan awal
                // routeId defaults to viewType
            }
        ]
    }
});

Ext.define('Admin.view.profile.Social', {
    extend: 'Ext.panel.Panel',
    xtype: 'profilesocial',//pin yang nantinya di panggil di (navigationtree.js)

    //sebagai kumpulan function yang akan di panggilkan pada halaman ini
    requires: [
        'Ext.Container' //bingkai dari profile
    ],

    //untuk mengatur posisi dan tipe dalam tampilan templates
    layout: {
        type: 'vbox',
        align: 'middle'
    },

    height: 320,
    
    bodyPadding: 40,
    
    //isi item yang akan di tampilkan pada saat browser di compile
    items: [
        {   //item bertipe image
            xtype: 'image',
            cls: 'userProfilePic', //sebagai class yang digunakan sebagai tema
            height: 150,
            width: 150,
            alt: 'profile-picture', //bingkai photo
            src: 'resources/images/user-profile/mitul.jpg'//photo
        },
        {   //komponen
            xtype: 'component',
            cls: 'userProfileName', //sebagai class yang digunakan tema
            height: '',
            html: 'Imbang Nugraha, S.Kom.' //konten
        },
        
        {   //komponen
            xtype: 'component',
            cls: 'userProfileDesc', // sebagai class yang di gunakan tema
            html: 'Karyawan Institut Teknologi Al Kamal'//konten
        }
    ]
});

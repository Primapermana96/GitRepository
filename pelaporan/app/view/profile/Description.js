Ext.define('Admin.view.profile.Description', {
    extend: 'Ext.Panel',
    xtype: 'profiledescription', // sebagai pin untuk di panggil di navigation tree

    //sebagai kumpulan function yang akan di panggilkan pada halaman ini
    requires: [
        'Ext.Button',
        'Ext.Img'
    ],

    //untuk mengatur posisi dan tipe dalam tampilan templates
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    // sebagai class yang di gunakan sebagai tema (Biodata)
    cls: 'timeline-items-wrap user-profile-desc',

    height: 300,

    items: [

            //sebagai komponen dari Description
        {
            xtype: 'component',
            baseCls: 'box x-fa fa-home', //iconnya
            html: 'Jalan Kedoya Raya Blok B2 no 10', //isinya
            padding: '0 0 8 0'//posisi konten
        },
        {   //sebagai komponen dari Description
            xtype: 'component',
            baseCls: 'box x-fa fa-clock-o', //iconnya
            html: 'Karyawan Institut Sains Teknologi Al Kamal',//isinya
            padding: '0 0 12 0'// posisi konten
        },
        {   //sebagai komponen dari Description
            xtype: 'component',
            baseCls: 'x-fa fa-inbox',//iconnya
            html: '<a href="#"\'>https://Imbang.Nugraha@gmail.com</a>',//isinya
            padding: '0 0 0 10'// posisi konten
        },
        {   //sebagai komponen dari Description
            xtype: 'component',
            baseCls: 'box x-fa fa-facebook',//iconnya
            html: '<a href="#"\'>https://www.facebook.com/ImbangNugraha</a>',//isinya
            padding: '0 0 12 0'// posisi konten
        },
        {   //sebagai komponen dari Description
            xtype: 'container',
            flex: 1, //pembatas 
            cls: 'about-me-wrap',
            html: '<h3 class="x-fa fa-user">Tentang Saya</h3><p>Saya adalah karyawan Institut Sains Teknologi Al Kamal dan saya sedang melanjutkan studi untuk mendapatkan gelar S2 di ITB</p>'
        }
    ]
});
